package com.example.microsoft.minions;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.content.Context;
import java.util.List;

import android.widget.TextView;
import android.widget.Toast;

import com.example.microsoft.minions.MAdapter.RecyclerItemClickListener;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    private List<data> Laps;
    private Context context;

    TextView min_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        min_name= (TextView) findViewById(R.id.minion_Name);
       // min_name.setText(getIntent().getStringExtra());

        RecyclerView rv = (RecyclerView) findViewById(R.id.rv);
        rv.addOnItemTouchListener(
                new RecyclerItemClickListener(context, rv ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // do whatever
                        Toast.makeText(MainActivity.this,
                              min_name.getText().toString()  , Toast.LENGTH_LONG).show();
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );


        rv.setHasFixedSize(true);

        initializeData();

        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);

        MAdapter adapter = new MAdapter(Laps);
        rv.setAdapter(adapter);


        

    }

    private void initializeData() {
        Laps = new ArrayList<>();
        Laps.add(new data("Kevin", R.drawable.kevin));
        Laps.add(new data("Dave", R.drawable.dave));
        Laps.add(new data("Jerry", R.drawable.jerry));
        Laps.add(new data("Mark", R.drawable.mark));
        Laps.add(new data("Phil", R.drawable.phil));
        Laps.add(new data("Stuart", R.drawable.stuart));
        Laps.add(new data("Tom", R.drawable.tom));
        Laps.add(new data("BOB", R.drawable.bob));



    }
}
